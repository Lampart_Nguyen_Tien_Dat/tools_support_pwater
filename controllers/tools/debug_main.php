<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class debug_main extends PW_Controller {

    protected $_layout = 'staff';
    public $segment_start = 5;
    protected $_title = 'tools';
    protected $db = '';
    /**
     * Contruct
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('date', 'array', 'file', 'form', 'download', 'url'));
        $this->load->library(array('session', 'zip', 'PW_Zip_file', 'pagination'));
        $this->load->model('order');

        // append tools to menu navigation
        $tools = '<a href="/_staff/tools/debug_cron">Cron</a></li><li><a href="/_staff/tools/debug_shipping_file">Shipping file</a></li><li><a href="/_staff/tools/debug_import_file">Import file</a>';
        echo "<script src='//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
        <script type='text/javascript'>
        $(document).ready(function(){"."$('#navigation ul').append('<li>$tools</li>');});
        </script>";

        error_reporting(E_ERROR | E_PARSE);
    }

    protected function _set_template() {
        $view = 'tools/';
        if ($this->view == null) {
            $view .= $this->_controller."/debug_". $this->_function;
            $this->view($view);
        } else {
            $view .= $this->view;
            $this->view($view);
        }
    }
}

/* End of file tools.php */
/* Location: {module_location}/_staff/tools/debug_main.php */