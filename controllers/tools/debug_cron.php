<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . '/debug_main.php');
class debug_cron extends debug_main{

    protected $_layout = 'staff';

    protected $_title = 'Staff cron';

    public function __construct() {
        parent::__construct();
        $this->db = new SQLite3('./application/widgets/_staff/controllers/tools/data/debug.db');
    }

    /**
     * Show and select cron
     *
     */
    public function index() {
        //Redirect to log in if not a user
        if((!$user = $this->session->userdata('user')) || (!$this->check_user($user))){
            redirect(base_url('_staff/tools/debug_cron/login'));
        }
        //Redirect to log in if log out
        if($this->get_var('logout') == "true"){
            $this->session->unset_userdata('user');
            redirect(base_url('_staff/tools/debug_cron/login'));
        }

        //Load database
        $this->check_db_vs_env($this->db);

        //Process in screen
        $controller = get_controller_names_by_module('cron');
        $this->assign('menu', $this->menu_items($controller));
        $this->show_cron();
        if($this->input->post("submit")) {
            $param = array(
                'account' => $user['id'],
                'db' => $this->db,
                'func' => $_POST[name],
                'qc_name' => $_POST[qc_name]
            );
            if ($code = $this->insert_hash_code($param)) {
                $link = $this->create_url(module('staff') . '/tools/debug_cron/run_cron', array("code" => $code));
                $link = "<a href='$link' style='color:#fc72ff; font-size: x-large'>Link chạy cron</a>";
                $this->assign('link', $link);
            }
        }

        //Log out button
        $logout_url = $this->create_url(module('staff').'/tools/debug_cron/index', array("logout" => "true"));
        $logout = "<div align='right' ><a href='$logout_url' style='background-color:#fc72ff; color:white'><i class='icon_logout icon_white'></i>Log out</a></div>";
        $this->assign('logout', $logout);

        $this->_set_template();
    }

    /**
     * Login
     * if has account, redirect to index
     *
     */
    public function login(){
        if($_POST[id] && $_POST[password]){
            $user = array('id' => $_POST[id], 'password' => base64_encode($_POST[password]));
            if($this->check_user($user)){
                $this->session->set_userdata('user', $user);
                redirect(base_url('_staff/tools/debug_cron/index'));
            }else {
                $this->assign('wrong','The username or password is incorrect.');
            }

        }
        $this->_set_template();
    }

    /**
     * Check user has account
     * @param
     */
    private function check_user($user){
        $query = $this->db->query("SELECT * FROM account WHERE login_id = '$user[id]' AND password = '$user[password]' AND disable = 0");
        if(!is_array($query->fetchArray())){
            return false;
        }
        return true;
    }

    /**
     * Gerenate menu cron
     * @return ul list of cron
     */
    private function menu_items($menu){
        $out = '<ul class="grade_1">';
        foreach($menu as $key => $value) {
            $out .= "<li class='item_menu' id='$value'>$value</li>";
        }
        $out .= '</ul>';
        return $out;
    }

    /**
     * Show function when select cron
     * @return list function of selected cron
     */
    private function show_cron(){
        $crons = get_controller_names_by_module('cron');
        $func_array = array();
        foreach ($crons as $cron){
            $funcs = get_action_names_by_controller('cron', $cron);
            foreach ($funcs as $func) {
                $func_array[$cron][] = $func;
            }
        }

        $content = "<div class='func'>";
        foreach ($func_array as $key => $items){
            foreach ($items as $item) {
                $content .= "<p class='$key' id='$item' style='display: none'><input type='checkbox' name='name[]' value='$item'>$item</p>";
            }
        }
        $content.= "</div>";
        $this->assign('content', $content);
    }

    /**
     * Gerenate hash for selected function in cron
     * then insert to table hash code
     * @param: $param: array
     * @return String: code to run cron
     *
     */
    private function insert_hash_code($param){
        $code = md5(random_string());
        $date_time = date("Y-m-d H:i:s");
        foreach ($param['func'] as $item){
            $query = $param['db']->query("SELECT id FROM cron WHERE function = '$item' AND disable = 0");
            $cron_id = $query->fetchArray(SQLITE3_ASSOC);
            if($param['db']->exec("INSERT INTO hash_code(account, cron_id, code, qc_name, date_time) VALUES('$param[account]', $cron_id[id], '$code', '$param[qc_name]', '$date_time')")){
                $insert = true;
            }
        }
        if(false == $insert){
            return false;
        }
        return $code;
    }

    /**
     * Show crons and excecute them
     */
    public function run_cron(){
        $code = $this->get_var('code');
        $success ='';
        $query = $this->db->query("SELECT id, cron_id FROM hash_code WHERE code = '$code'");
        while ($query_result = $query->fetchArray(SQLITE3_ASSOC)){
            $data_hashcode[] = $query_result;
        }
        $this->process_cron($data_hashcode);
        $param = array(
            'db' => $this->db
        );

        if($_POST[cron] && $_POST[func]){
            $order_detail_id = trim($_POST['order_details']);
            $string_execute = "";
            if(!empty($order_detail_id)){
                $order_detail_id = '"' . $order_detail_id . '"';
                $string_execute = "/usr/bin/php " . FCPATH . "index.php" ." cron/$_POST[cron] $_POST[func] $order_detail_id";
            }else{
                $string_execute = "/usr/bin/php " . FCPATH . "index.php" ." cron/$_POST[cron] $_POST[func]";
            }
            if (is_string($_POST['ext_param']) && !empty($_POST['ext_param'])) {
                $string_execute .= ' ' . $_POST['ext_param'];
            }
            exec($string_execute, $output, $result);
            $result = "Chạy cron $_POST[func] thành  công" . "</br>";
            $result .= $string_execute . "</br>";
            foreach ($output as $string){
                $result .= $string;
                $result .=  "<br>";
            }
            $param['result'] = 'failed';
            $param['hash_code_id'] = $_POST['hash_code_id'];
            $param['func'] = $_POST['func'];
            $this->insert_log_run_cron($param);
        }

        if($_POST['contract_cycle']){
            $contract_ids = explode(',', trim($_POST['contract_cycle']));
            foreach ($contract_ids as $contract_id) {
                if ($this->order->create_cycle_order_not_send_purchase($contract_id)) {
                    $success .= $contract_id;
                }
            }
            $result = "Sucess: $success";
//             $controller = Modules::load('cron/process_carrier');
//             call_user_func(array($controller, 'purchase_cyclic_delivery'));
        }


        $this->assign('result', $result);
        $this->_set_template();
    }

    /**
     * show crons with params
     * @param $param
     */
    private function process_cron($param){
        $result ='';

        foreach ($param as $item){
            $query = $this->db->query("SELECT cron, function FROM cron WHERE id = $item[cron_id]");
            while($query_result = $query->fetchArray(SQLITE3_ASSOC)){
                $param = array(
                    'cron' => $query_result[cron],
                    'func' => $query_result['function'],
                    'hash_code_id' => $item['id']
                );
                $result .= $this->create_contents( $param);
            }
        }
        $this->assign('form', $result);
    }

    /**
     * Create content to show cron
     * @param: $param: array
     * @return content crons
     */
    private function create_contents($param){
        $result = '<form action ="" method="post">';
        if($param[cron] == "contract_cron" && $param[func] == "do_create_order"){
            $result .= "<div align='center'>
                    <h1 style='border-bottom:none'>Cron $param[func]</h1>
                    <label style='line-height: 3'>contract_ids</label>
                    <input type='textbox' name='contract_cycle'/><br>
                    <input type='hidden' value='$param[hash_code_id]' name='hash_code_id'>
                    <button type='submit'class = 'submit_button blue br4 mr10'>Click to run cron!</button>
                    </div></br></br>";
        }elseif($param[cron] == "process_amazon" && ( $param[func]== "authorizeOnBillingAgreement" || $param[func]== "amazon_pay_capture_single_fee")){
            $result .= "<div align='center'>
                    <h1 style='border-bottom:none'>Cron $param[func]</h1>
                    <label style='line-height: 3'>order_detail_id: 716231_716232_716233</label>
                    <input type='textbox' name='order_details'/><br>
                    <input type='hidden' value='$param[hash_code_id]' name='hash_code_id'>
                    <input type='hidden' value='$param[cron]' name='cron'>
                    <button type='submit' name='func' value='$param[func]' class = 'submit_button blue br4 mr10'>Click to run cron!</button>
                    </div></br></br>";
        }else{
            $result .= "<div align='center'>
                    <h1 style='border-bottom:none'>Cron $param[func] <input type='text' name='ext_param' placeholder='Extra parameter (optional)'></h1>
                    <input type='hidden' value='$param[cron]' name='cron'>
                    <input type='hidden' value='$param[hash_code_id]' name='hash_code_id'>
                    <button type='submit' name='func' class = 'submit_button blue br4 mr10' value='$param[func]'>Click to run cron!</button>
                    </div></br></br>";
        }
        $result .= '</form>';
        return $result;
    }

    /**
     * Compare data in table cron vs controllers in module cron
     * Update to database if difference
     * @param: $db: String name of db
     * @return true or update db
     */
    private function check_db_vs_env($db){

        $env_cron = get_controller_names_by_module('cron');
        $exception_list = array('notify_mizuho_to_cod_mail', 'notify_mizuho_not_shipment_mail');
        $env_cron = array_diff($env_cron, $exception_list);
        $query = $db->query("SELECT distinct cron FROM cron");
        while($query_result = $query->fetchArray(SQLITE3_ASSOC)){
            $db_cron[] = $query_result['cron'];
        }

        foreach ($env_cron as $cron){
            $env_func = get_action_names_by_controller('cron', $cron);

            $query = $db->query("SELECT function FROM cron");
            while($query_result = $query->fetchArray(SQLITE3_ASSOC)){
                $db_func[] = $query_result['function'];
            }

            foreach ($env_func as $func){
                if(!in_array($func, $db_func)){
                    $update[$cron][] = $func;
                }
            }
        }

        if($update){
            //Update cron doesn't exist in table cron in database
            foreach ($update as $key => $item){
                foreach ($item as $val){
                    $db->exec("INSERT INTO cron(cron, function) VALUES('$key', '$val')");
                    }
                }
        }else {
            return true;
        }
    }

    /**
     * @param $param
     * @return bool
     */
    private function insert_log_run_cron($param){
        $date_time = date("Y-m-d H:i:s");
        $return = false;
        $query = $param['db']->query("SELECT account, qc_name FROM hash_code  WHERE id = $param[hash_code_id] AND disable = 0");
        $query_result = $query->fetchArray(SQLITE3_ASSOC);
        $param['account'] = $query_result['account'];
        $param['qc_name'] = $query_result['qc_name'];

        if($param['db']->exec("INSERT INTO log_run_cron(function, account, qc_name, hash_code_id, result, create_datetime) 
                              VALUES('$param[func]', '$param[account]', '$param[qc_name]', $param[hash_code_id], '$param[result]', '$date_time')")){
            $return = true;
        }
        return $return;
    }

    /**
     * show log cron
     */
    public function show_log_cron(){
        $query = $this->db->query("SELECT COUNT() AS count FROM log_run_cron ");
        $count = $query->fetchArray(SQLITE3_ASSOC);
        $config['base_url'] = base_url('_staff/tools/debug_cron/show_log_cron');
        $config['total_rows'] = $count['count'];
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $this->_set_template();
        $param = array();
        if(isset($_POST['pg'])){
            $param = $this->_get_data($config['per_page'], $_POST['pg']);
        }else{
            $param =  $this->_get_data($config['per_page']);
        }
        $this->assign('param', $param);
        $this->assign('pagination', $this->pagination->create_links());
    }

    /**
     * get data to pagination
     * @param int $limit
     * @param int $offset
     * @return array
     */
    private function _get_data($limit, $offset = 0){
        $param = array();
        $query = $this->db->query("SELECT qc_name, function, create_datetime, result, account FROM log_run_cron LIMIT $limit  OFFSET $offset");
        while($query_result = $query->fetchArray(SQLITE3_ASSOC)){
            $param[] = $query_result;
        }
        return $param;
    }

}
/* End of file cron.php */
/* Location: {module_location}/staff/tools/debug_cron.php */