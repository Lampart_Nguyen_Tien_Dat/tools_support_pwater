<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once (dirname(__FILE__) . '/debug_main.php');
class debug_import_file extends debug_main{

    protected $_layout = 'staff';

    protected $_title = 'tools';

    const _SHIPPING_SAGAWA = 8;
    const _SHIPPING_YAMATO = 9;
    const _SHIPPING_YHS = 10;
    const _SHIPPING_KFG = 31;
    const _FILE_EXTENSION = '.csv';

    public function index(){
        if($_POST[order_detail_id]){
            $order_detail_ids = explode(',', $_POST[order_detail_id]);
            $file_path = 'application/widgets/_staff/controllers/tools/data/';
            $file_name_zip = 'shipping_file.zip';
            foreach ($order_detail_ids as $order_detail_id){
                if(!$order_data = $this->order->get_order_details_by_id(trim($order_detail_id))){
                    $result = "ahihi, không có dữ liệu order ứng với order_detail_id: $order_detail_id nha";
                    break;
                }
                if(!$contract_data = $this->order->get_contract_info_by_order_detail_id($order_detail_id)){
                    $result = "ahihi, không có dữ liệu contract ứng với order_detail_id: $order_detail_id nha";
                    break;
                }
                $order_data = $order_data[0];
                $contract_data = $contract_data[0];
                $data_for_sagawa_yamato = $this->_get_data_for_sagawa_yamato($contract_data[contract_id], $order_data[order_detail_no]);
                $order_settlement = $this->_get_order_settlement_by_order_detail_id($order_detail_id);
                $header_sagawa = "レコード区分,依頼日,出荷日,配達指定日,時間帯指定,商品総金額,代引手数料,代引金額,合計消費税額,出荷総個口数,顧客コード,荷受人 住所１,荷受人　住所２,荷受人　住所３,荷受人　名称１,荷受人　名称２,荷受人　電話番号,備考１,備考２,シリアル番号,お客様番号（＝識別番号）,注文番号,出荷番号,問合せ番号,佐川急便配達店コード\r\n"
                        ."レコード区分,注文番号,出荷番号,商品ｎ【品番】,商品ｎ【品名】,商品ｎ【数量】,商品ｎ【単価】,商品ｎ【合計金額】,商品ｎ【個口数】,発行日時,発行端末\r\n";
                $header_yamato = 'レコード区分,"依頼日","集荷希望日","着荷指定日","着荷指定時刻条件","商品総金額（税込み）","代引き手数料（税込み）","品代金","合計消費税額","個数(依頼）","荷送人ｺｰﾄﾞ","荷受人住所(漢字）１","荷受人住所(漢字）２","荷受人住所（漢字）３","荷受人名(漢字）","荷受人名２(漢字）","荷受人電話番号","キャンセルフラグ","備考２","シリアル番号","荷受人ｺｰﾄﾞ","運送依頼番号","出荷番号"\r\n'
                        .'レコード区分,"注文番号","出荷番号","商品ｎ【品番】","商品ｎ【品名】","商品ｎ【数量】","商品ｎ【単価】","商品ｎ【合計金額】","商品ｎ【個口数】","伝票番号（問合せ番号）"\r\n';
                $header_yhs = "出荷日,問合番号,元着,便種,配達指定日,代引金額,営止指定,出荷個数,運賃請求コード,荷送人コード,荷受人コード,荷受人・名称１,荷受人・名称２,荷受人・住所・都道府県,荷受人・住所・市区町村,荷受人・住所・町名番地１,荷受人・住所・町名番地２,荷受人・電話番号,コメント,記事１,記事２,記事３,記事４,記事５,ＪＩＳコード,着店コード,荷送人側管理番号１,荷送人側管理番号２,記事６,記事７,記事８,記事９,記事１０,記事１１,記事１２,運賃,メールアドレス,重量１,重量２,重量３,重量４,重量５,重量６,重量７,個数１,個数２,個数３,個数４,個数５,個数６,個数７,時間帯指定,記事１３,記事１４,記事１５,記事１６,記事１７,記事１８,荷送人・名称１,荷送人・名称２,荷送人・住所１,荷送人・住所２,荷送人・住所３,荷送人・電話番号\r\n";
                $file_name[sagawa] = 'sagawa';
                $file_name[yamato] = 'yamato';
                $file_name[yhs] = 'yhs';
                $file_name[kfg] = 'kfg';
                switch ($order_data[shipping_file_id]){
                    case self::_SHIPPING_SAGAWA:
                        $file_name[sagawa] .= "_$order_detail_id";
                        $data_sagawa .= 'H,"20160529","$order_settlement[sale_date]","20160531","01","7344","0","0","544","2","118192081003","埼玉県川口市芝下2-5-1","","","株式会社　イーストナイン","","048-261-1981","","","$data_for_sagawa_yamato[serial_no]","Customer number","$data_for_sagawa_yamato[order_detail_no]","Shipment number","","Sagawa Express delivery shop code"'
                                         .'D,"$data_for_sagawa_yamato[order_detail_no]","Shipment number","$data_for_sagawa_yamato[product_code]","$data_for_sagawa_yamato[product_code_name]","1","3672","7344","2","2014/08/13 19:25:39","LVIS"';
                        break;
                    case self::_SHIPPING_YAMATO:
                        $file_name[yamato] .= "_$order_detail_id";
                        $data_yamato = 'H,"20160529","$order_settlement[sale_date]","20160531","04","0","0","","0","003","118192081003(shipper_code)","愛知県一宮市神山3-2-16ビーライン","森102号","","タヤ　翔子","","080-9724-6832","","","$data_for_sagawa_yamato[serial_no]","consignor_code","$data_for_sagawa_yamato[order_detail_no]","Shipment number"\r\n'
                                       .'D,"$data_for_sagawa_yamato[order_detail_no]","Shipment number","$data_for_sagawa_yamato[product_code]","$data_for_sagawa_yamato[product_code_name]","2","0","0","1",\r\n';
                        break;
                    case self::_SHIPPING_YHS:
                        $file_name[yhs] .= "_$order_detail_id";
                        $data_yhs = "10342202,4.64E+11,090-9654-2517,赤嶺　いより,9011113,沖縄県南風原町字喜屋武６,東宝２７-２０３,098-840-3525,沖縄物流システム支店内　プレミアムウォーター㈱倉庫,"
                                ."$order_detail_id,沖縄県糸満市西崎町4-21-3 沖縄物流システム支店内,,3001211106,,0,クール以外,,,,,,,,,,未配完,,,,,,,,,,指定なし\r\n";
                        break;
                    case self::_SHIPPING_KFG:
                        $file_name[kfg] .= "_$order_detail_id";
                        $contract_id = $contract_data[contract_id];
                        $instruction_plan_date = $order_data[instruction_plan_date];
                        $data_kfg = "$instruction_plan_date,5.70E+11,10,0,209,,,1,1.54E+11,1.54E+11,,石中　愛理,,石川県,小松市,古河町９９番地ハーミット・クラブ,古河２０１,090-8099-6114,,1153100002,プレミアムウォーター ペットタイプ 金城,,,,17203,5月13日,$contract_id,$order_detail_id,,,0,プレミアムウォーター株式会社,東京都渋谷区神宮前１－４－１６　神宮前Ｍ－ＳＱＵＡＲＥ　３Ｆ,0120-937-032,2,0,,1,2,5,10,20,30,0,0,0,0,0,0,0,0,18,,,,,,,プレミアムウォーター株式会社,,東京都　渋谷区,神宮前１－４－１６,神宮前Ｍ－ＳＱＵＡＲＥビル３Ｆ,0120-937-032\r\n";
                        break;
                    default:
                        $result = 'fail';
                }
            }
            if(isset($data_sagawa)){
                write_file($file_path.$file_name[sagawa].self::_FILE_EXTENSION, $header_sagawa.$data_sagawa);
            }
            if(isset($data_yamato)){
                write_file($file_path.$file_name[yamato].self::_FILE_EXTENSION, $header_yamato.$data_yamato);
            }
            if(isset($data_yhs)){
                write_file($file_path.$file_name[yhs].self::_FILE_EXTENSION, $header_yhs.$data_yhs);
            }
            if(isset($data_kfg)){
                write_file($file_path.$file_name[kfg].self::_FILE_EXTENSION, $data_kfg);
            }
            $zip = new ZipArchive;
            $count = 0;
            $files = scandir($file_path);
            foreach($files as $file) {
                if(false !== strpos($file, 'sagawa') || false !== strpos($file, 'yamato') || false !== strpos($file, 'yhs') || false !== strpos($file, 'kfg')) {
                    if ($zip->open($file_path.$file_name_zip, ZipArchive::CREATE)) {
                        $zip->addFile($file_path.$file, $file);
                        $zip->close();
                        ++$count;
                    }
                }
            }
            if($count) {
                download_file($file_name_zip, $file_path.$file_name_zip, true);
            }
        }
        $this->assign('result', $result);
        $this->_set_template();
    }

    private function _get_data_for_sagawa_yamato($contract_id, $order_detail_no){
        $sql = "select contract.id,
                contract_order.order_type_code,
                shipping_file.column_name,
                order_product.product_code_id,
                order_product.product_code_name,
                 order_detail.order_detail_no,
                  order_product.product_code,
                  order_product.product_name,
                  order_product.remarks
                ,
                Case
                                    when product.resource_type = 1 then 'water'
                                    when product.resource_type = 2 then 'server'
                                    when product.resource_type = 3 then 'holder'
                                    when product.resource_type = 4 then 'option_product'
                            End
                            AS
                                `type`,
                                server_serial.serial_no
                from contract

                join contract_order on contract.id = contract_order.contract_id
                join order_detail on contract_order.id = order_detail.contract_order_id
                join order_product on order_detail.id = order_product.order_detail_id
                inner join product on order_product.product_id = product.id
                left join water on product.resource_id = water.id and product.resource_type = 1
                left join server on product.resource_id = server.id and product.resource_type = 2
                left join holder on product.resource_id = holder.id and product.resource_type = 3
                left join option_product on product.resource_id = option_product.id and product.resource_type = 4
                left join server_serial on server_serial.product_id = product.id and product.resource_type = 2 and server_serial.server_status_id = 101
                join shipping_file on order_detail.shipping_file_id = shipping_file.id
                where contract.id  = $contract_id
                and order_detail.order_detail_no like '$order_detail_no'";
        $query = $this->db->query($sql);
        $result = array();
        foreach ($query->result_array() as $item){
            if(!in_array($item, $result)){
                $result[] = $item;
            }
        }
        return $result;
    }

    private function _get_order_settlement_by_order_detail_id($order_detail_id){
        $sql = "SELECT *
                 FROM order_settlement
                 WHERE order_detail_id = $order_detail_id";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        return $result;
    }
}
/* End of file tools.php */
/* Location: {module_location}/_staff/tools/debug_import_file.php */
