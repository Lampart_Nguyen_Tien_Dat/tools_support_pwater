<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(dirname(__FILE__) . '/debug_main.php');
class create_import_file extends debug_main{
    
    protected $_error = array();
    
    protected $_layout = 'staff';
    
    protected $_title = 'tools';
        
    const _FILE_EXTENSION = '.csv';
    
    const COMPANY_KEY_YAMATO = 'YAMATO';
    const COMPANY_KEY_SAGAWA ='SAGAWA';
    const COMPANY_KEY_MEISUI ='MEISUI';
    const COMPANY_KEY_YHS ='YHS';
    const COMPANY_KEY_KNG ='KNG';
    
    const CONFIG_HEADER = 'HEADER';
    const CONFIG_DATA = 'DATA';
    const CONFIG_DETAIL = 'DETAIL';
    const CONFIG_TYPE_H = 'H';
    const CONFIG_TYPE_D = 'D';    
    const CONFIG_MAX_COL = 'MAX_COL';
    
    const AUTO = 'AUTO';
    
    const ENTER_CHAR = "\r\n";
    
    protected $db = null;
    
    function __construct() {
        parent::__construct();
        $CI =& get_instance();
        $this->db = $CI->db;
        $this->load->model(array(
            'delivery_company',
            'settlement_type',
        ));
    }

    /**
     *
     * @param string $company_key //['YAMATO', 'SAGAWA', 'MEISUI', 'YHS', 'KNG']
     * @param array $data // array [order_detail_info, 'order_product' => [order_product_info]]
     * @param string $type // 'HEADER', 'H', 'D'
     */
    private function _get_build_config() {
        $buildConfig = array(
            self::COMPANY_KEY_YHS => array(
                self::CONFIG_DATA       => array(
                    self::CONFIG_TYPE_D  => array(
                        self::CONFIG_MAX_COL    => 36,
                        self::CONFIG_DETAIL     => array(
                            0       => 'order_detail_no',
                            1       => 'confirm_no',
                            3       => 'customer_name',
                            5       => 'address_1',
                            6       => 'address_2',
                        )
                     )
                )
            ),
            self::COMPANY_KEY_KNG => array(
                self::CONFIG_HEADER     => self::AUTO,
                self::CONFIG_DATA       => array(
                    self::CONFIG_TYPE_D  => array(
                        self::CONFIG_MAX_COL    => 64,
                        self::CONFIG_DETAIL     => array(
                            26      => 'contract_id',
                            27      => 'order_detail_no' ,
                            // 0       => 'shipping_date',
                            0       => 'sales_date',
                            4       => 'delivery_date',
                            1       => 'delivery_confirm_no',
                            2       => 'settlement_ways',
                            3       => 'delivery_types',
                            // address
                            11      => 'address_1',
                            12      => 'address_2',
                            // customer receive
                            13      => 'city',
                            14      => 'county',
                            15      => 'street_name',
                            16      => 'house_number',
                            17      => 'telephone',
                            19      => 'product_code',
                            20      => 'product_name',
                            // set quantity
                            37      => 'set',
                            38      => 'bottle',
                            7       => 'packages',
                        )
                    )
                )
            ),
            self::COMPANY_KEY_YAMATO => array(
                self::CONFIG_HEADER     => self::AUTO,
                self::CONFIG_DATA       => array(
                    self::CONFIG_TYPE_H  => array(
                        self::CONFIG_MAX_COL    => 23,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            21      => 'order_detail_no',
                            2       => 'sales_date',
                            19      => 'server_serial',
                            3       => 'first_use_date',
                            11      => 'address_1',
                            12      => 'address_2',
                            13      => 'address_3',
                        )
                    ),
                    self::CONFIG_TYPE_D  => array(
                        self::CONFIG_MAX_COL    => 23,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            1       => 'order_detail_no',
                            3       => 'product_code',
                            9       => 'confirm_no',
                            5       => 'product_quantity',
                            4       => 'product_name',
                            8       => 'packages',
                        )
                    )
                )
            ),
            self::COMPANY_KEY_SAGAWA => array(
                self::CONFIG_HEADER     => self::AUTO,
                self::CONFIG_DATA       => array(
                    self::CONFIG_TYPE_H  => array(
                        self::CONFIG_MAX_COL    => 25,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            21      => 'order_detail_no',
                            2       => 'sales_date',
                            19      => 'server_serial',
                            3       => 'first_use_date',
                            23      => 'confirm_no',
                            11      => 'address_1',
                            12      => 'address_2',
                            13      => 'address_3',
                        )
                    ),
                    self::CONFIG_TYPE_D  => array(
                        self::CONFIG_MAX_COL    => 25,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            1       => 'order_detail_no',
                            3       => 'product_code',
                            5       => 'product_quantity',
                            4       => 'product_name',
                            8       => 'packages',
                        )
                    )
                )
            ),
            self::COMPANY_KEY_MEISUI => array(
                self::CONFIG_HEADER     => self::AUTO,
                self::CONFIG_DATA       => array(
                    self::CONFIG_TYPE_H  => array(
                        self::CONFIG_MAX_COL    => 19,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            18      => 'order_detail_no',
                            1       => 'sales_date',
                            2       => 'first_use_date',
                            9       => 'address_1',
                            10      => 'address_2',
                            11      => 'address_3',
                        )
                    ),
                    self::CONFIG_TYPE_D  => array(
                        self::CONFIG_MAX_COL    => 19,
                        self::CONFIG_DETAIL     => array(
                            0       => 'record_type',
                            1       => 'order_detail_no',
                            2       => 'product_code',
                            8       => 'confirm_no',
                            9       => 'server_serial',
                            4       => 'product_quantity',
                            3       => 'product_name',
                            7       => 'packages',
                        )
                    )
                )
            )
        );
        
        return $buildConfig;
    }
    
    public function index(){
        //		error_reporting(E_ALL);
        //        ini_set('display_errors', 1);
        $result = '';
        if(!empty($_POST['order_detail_id'])){
            
            $order_detail_ids = explode(',', $_POST['order_detail_id']);
            $file_path = APPPATH.'/widgets/_staff/controllers/tools/data/';
            $file_name_zip = 'shipping_file-'.time().'.zip';
            
            $datas = $this->_get_data_import($order_detail_ids);
            
            $data_group_by_company = $this->_group_by_data($datas);
            
            $csv_content_array = $this->_build_content_csv($data_group_by_company);
            
            $zip = new ZipArchive();
            if ($zip->open($file_path.$file_name_zip, ZIPARCHIVE::CREATE)!==TRUE) {
                exit("cannot open $file_name_zip\n");
            }
            foreach ($csv_content_array as $company => $data) {
                $zip->addFromString($company.".csv", mb_convert_encoding($data, "sjis-win", "UTF-8"));
            }
            if ($this->_error) {
                $zip->addFromString("error.txt", implode(self::ENTER_CHAR, $this->_error));
            }
            $zip->close();
            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename=$file_name_zip");
            header("Content-length: " . filesize($file_path.$file_name_zip));
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile($file_path.$file_name_zip);
            unlink($file_path.$file_name_zip);
            die;
        }
        
        if (!empty($this->_error)) {
            $result .= implode('<br/>', $this->_error);
        }
            
        $this->assign('result', $result);
        $this->assign('delivery_types', $this->delivery_company->delivery_types);
        $this->assign('settlement_ways', $this->settlement_type->settlement_ways);
        $this->_set_template();
    }
    
    private function _build_content_csv ($data_group_by_company) {        
        if (!is_array($data_group_by_company) || empty($data_group_by_company)) {
            return array();
        }
        $buildConfig = $this->_get_build_config();
        
        $csv_content_array = array();
        foreach ($data_group_by_company as $company_key => $order_detail_list) {            
            if (empty($buildConfig[$company_key]) || empty($buildConfig[$company_key][self::CONFIG_DATA])) {
                continue;
            }
            $company_config = $buildConfig[$company_key];
            
            // Create header
            if (!isset($csv_content_array[$company_key])) {
                $csv_content_array[$company_key] = '';
                if (isset($company_config[self::CONFIG_HEADER])) {
                    if ($company_config[self::CONFIG_HEADER] == self::AUTO) {
                        foreach ($company_config[self::CONFIG_DATA] as $current_config_data) {
                            $max_col = null;
                            $data_detail = null;

                            // get MAX_COL from root config or H type config
                            if (!empty($current_config_data[self::CONFIG_MAX_COL])) {
                                $max_col = $current_config_data[self::CONFIG_MAX_COL];
                            }
                            // get CONFIG_DATA from root config or H type config
                            if (!empty($current_config_data[self::CONFIG_DETAIL])) {
                                $data_detail = $current_config_data[self::CONFIG_DETAIL];
                            }

                            if ($max_col && $data_detail) {
                                $csv_content_array[$company_key] .= $this->_build_header_auto($max_col, $data_detail).self::ENTER_CHAR;
                            }
                        }
                        
                    } else if (is_string($company_config[self::CONFIG_HEADER])) {
                        $csv_content_array[$company_key] .= $company_config[self::CONFIG_HEADER];
                    }
                }
            }
            
            // Create Content
            if ($order_detail_list) {
                foreach ($order_detail_list as $order_data) {
                    $csv_content_array[$company_key] .= $this->_build_data($company_config[self::CONFIG_DATA], $order_data);
                }
            }
        }
        return $csv_content_array;
    }
    
    private function _build_data($config_data, $order_data) {
        $content_detail = '';
        if (!empty($config_data[self::CONFIG_TYPE_H])) {
            $first_data = reset($order_data);
            $content_detail .= $this->_buil_detail($config_data[self::CONFIG_TYPE_H], $first_data, array('record_type' => 'H')) . self::ENTER_CHAR;
        }
        
        if (!empty($config_data[self::CONFIG_TYPE_D])) {
            foreach ($order_data as $data) {
                $content_detail .= $this->_buil_detail($config_data[self::CONFIG_TYPE_D], $data, array('record_type' => 'D')) . self::ENTER_CHAR;
            }
        }
        
        return $content_detail;
    }
    
    private function _buil_detail($config_type, $data, $default = array()) {
        if (empty($config_type[self::CONFIG_MAX_COL]) || empty($config_type[self::CONFIG_DETAIL])) {
            return '';
        }
        
        $global_default = array(
            'confirm_no' => '12345678',
            'settlement_ways' => $_POST['settlement_ways'],
            'delivery_types' => $_POST['delivery_types'],
        );
        $global_replace = array(
            'sales_date'            => 'shipping_plan_date',
            'first_use_date'        => 'shipping_plan_date',
            'product_quantity'      => 'quantity',
            'bottle'                => 'quantity',
            'packages'              => 'set_quantity',
            'set'                   => 'set_quantity',
            'customer_name'         => 'name_1 name_2',
            'telephone'             => 'phone_no mb_phone_no',
            'city'                  => 'address_1',
            'county'                => 'address_1',
            'street_name'           => 'address_2',
            'house_number'          => 'address_3'
        );
        $default = array_merge($global_default, $default);
                
        $content_detail = array();
        $max_col = $config_type[self::CONFIG_MAX_COL];
        
        for ($i = 0; $i < $max_col; $i++) {
            $content_detail[$i] = '';
            if (isset($config_type[self::CONFIG_DETAIL][$i])) {
                $field = $config_type[self::CONFIG_DETAIL][$i];
                if (isset($data[$field])) {
                    $content_detail[$i] = $data[$field];
                }
                if (empty($content_detail[$i]) && !empty($default[$field])) {
                    $content_detail[$i] = $default[$field];
                }
                if (empty($content_detail[$i]) && !empty($global_replace[$field])) {
                    $content_detail[$i] = '';
                    $replace = explode(' ', $global_replace[$field]);
                    foreach ($replace as $field_name) {
                        $content_detail[$i] .= $data[$field_name];
                    } 
                }
                if (empty($content_detail[$i])) {
                    $this->_write_error('Empty field `'.$field.'` in order_detail: '.$data['order_detail_id']);
                }
            }
        }
        return implode(",", $content_detail);
    }
    
    private function _build_header_auto($max_col, $data_header) {
        if (!is_numeric($max_col) || $max_col < 1) {
            return '';
        }
        $pieces = array();
        for ($i = 0; $i < $max_col; $i ++) {
            if (isset($data_header[$i])) {
                $pieces[] = $data_header[$i];
            } else {
                $pieces[] = '';
            }
        }
        return implode(",", $pieces);
    }
    
    private function _get_shipping_file_map () {
        $shipping_map = array(
            self::COMPANY_KEY_YAMATO    => array('yamato', 'seino', 'bondsnet'),
            self::COMPANY_KEY_SAGAWA    => array('sagawa'),
            self::COMPANY_KEY_MEISUI    => array('meisui'),
            self::COMPANY_KEY_YHS       => array('yhs'),
            self::COMPANY_KEY_KNG       => array('kfg'),
        );
        $query = $this->db->query("SELECT * FROM `shipping_file` WHERE `disable` = 0");
        $shipping_id_map = array();
        foreach ($query->result_array() as $item){
            foreach ($shipping_map as $mKey => $map) {
                if ($this->_strposa($item['column_name'], $map) !== false) {
                    $shipping_id_map[$item['id']] = $mKey;
                    break;
                }
            }
        }
        return $shipping_id_map;
    }
    
    private function _group_by_data ($datas) {
        if (!is_array($datas) || empty($datas)) {
            return array();
        }
                
        $group_by_order_detail = array();
        foreach ($datas as $item){
            if (!isset($group_by_order_detail[$item['order_detail_id']])) {
                $group_by_order_detail[$item['order_detail_id']] = array();
            }
            $group_by_order_detail[$item['order_detail_id']][] = $item;
        }
        
        $shipping_file_id_map = $this->_get_shipping_file_map();
        
        $group_by_company = array();
        foreach ($group_by_order_detail as $order_detail_id => $data) {
            if (empty($data)) {
                continue;
            }
            $shipping_file_id = $data[0]['shipping_file_id'];
            if (empty($shipping_file_id_map[$shipping_file_id])) {
                continue;
            }
            $company_key = $shipping_file_id_map[$shipping_file_id];
            if (!isset($group_by_company[$company_key])) {
                $group_by_company[$company_key] = array();
            }
            $group_by_company[$company_key][$order_detail_id] = $data;
        }
        
        return $group_by_company;
    }
    
    private function _get_data_import ($order_detail_ids= array()) {
        $sql = "SELECT *, `server_serial`.`serial_no` as server_serial
                FROM `order_detail`
                INNER JOIN `contract_order` ON (`contract_order`.`id` = `order_detail`.`contract_order_id` AND `contract_order`.`disable` = 0)
                INNER JOIN `contract` ON (`contract`.`id` = `contract_order`.`contract_id` AND `contract`.`disable` = 0)
                INNER JOIN `order_product` ON (`order_detail`.`id` = `order_product`.`order_detail_id` AND `order_product`.`disable` = 0)
                INNER JOIN `product` ON (`product`.`id` = `order_product`.`product_id` AND `product`.`disable` = 0)
                INNER JOIN `shipping_file` ON (`shipping_file`.`id` = `order_detail`.`shipping_file_id` AND `shipping_file`.`disable` = 0)
                INNER JOIN `order_delivery` ON (`order_delivery`.`order_detail_id` = `order_detail`.`id` AND `order_delivery`.`disable` = 0)
                INNER JOIN `order_acquisition` ON (`order_acquisition`.`order_detail_id` = `order_detail`.`id` AND `order_acquisition`.`disable` = 0)
                INNER JOIN `partner` ON (`partner`.`id` = `order_acquisition`.`payment_partner_id` AND `partner`.`disable` = 0)
                LEFT JOIN `server_serial` ON (`server_serial`.`product_id` = `product`.`id` AND `product`.`resource_type` = 2 AND `server_serial`.`server_status_id` = 101)
                WHERE `order_detail`.`id` IN (".implode(",", $order_detail_ids).")
                GROUP BY `order_product`.`id`";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    private function _strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
    }
    
    protected function _write_error ($message) {
        if ($message) {
            $this->_error[] = $message;
        }
    }
}
/* End of file tools.php */
/* Location: {module_location}/_staff/tools/debug_import_file.php */
