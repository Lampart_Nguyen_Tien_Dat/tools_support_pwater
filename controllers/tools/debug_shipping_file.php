<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once(dirname(__FILE__) . '/debug_main.php');

class debug_shipping_file extends debug_main
{

    protected $_layout = 'staff';

    protected $_title = 'tools';

    public function index()
    {
        //Download shipping file for specific order_detail_id
//        if ($_POST['order_detail_id']) {
//            $update = $this->update_order_detail_to_download_shipping_file($_POST['order_detail_id']);
//
//            if ($update) {
//                $result = 'Update success: ' . $_POST['order_detail_id'];
//            } else {
//                $result = 'Update fail';
//            }
//        }

        //Modify shipping file for specific day
        if ($_POST['day']) {
            $file_name = str_replace('-', '', $_POST['day']) . '_shipping.zip';
            $down_file_name = 'data'
                . '/' . trim(config_item('shipping_file_download_subpath'), '/')
                . '/' . $file_name;

            if (file_exists($down_file_name)) {
                $new_file_name = str_replace('-', '', $_POST['day']) . '_shipping_backup_script_' . date('YmdHis') . '.zip';
                $new_down_file_name = 'data'
                    . '/' . trim(config_item('shipping_file_download_subpath'), '/')
                    . '/' . $new_file_name;
                rename($down_file_name, $new_down_file_name);
                $result = "Modified shipping file on" . $_POST['day'] . " from: " . $file_name . " to: " . $new_file_name;
            } else {
                $result = "No exist shipping file on : " . $_POST['day'];
            }
        }

        if (isset($result)) {
            $this->assign('result', $result);
        }
        $this->_set_template();
    }

    public function web_users_folder()
    {
        //Get folder
        $folder_list = array(
            '/web_users/wd/premium_water/shipping/plant/',
            '/web_users/asago/premium_water/shipping/plant/'
        );

        $except_name = array('.', '..', '.gitkeep');
        $result = '';
        foreach ($folder_list AS $folder) {
            $folder_detail = scandir(FCPATH . $folder, 1);

            if (null == $folder_detail) {
                continue;
            }
            $result .= "<ul><h2>$folder</h2>";
            foreach ($folder_detail AS $item) {
                if (in_array($item, $except_name)) {
                    continue;
                }
                if (is_dir(FCPATH . $folder . $item)) {
                    $sub_folder_detail = scandir(FCPATH . $folder . $item, 1);
                    $result .= "<li><ul>DIR: $item";
                    foreach ($sub_folder_detail AS $sub_item) {
                        if (in_array($sub_item, $except_name)) {
                            continue;
                        }
                        $result .= "<li>$sub_item</li>";
                    }
                    $result .= "</ul></li>";
                } else {
                    $result .= "<li style='cursor: pointer; display: table;' class='zip " . $folder . "'>$item</li>";
                }
            }
            $result .= '</ul>';
        }

        //Render
        $this->assign('result', $result);
        $this->_set_template();
    }

    public function download_file()
    {
        $path = $this->get_var('path');
        $path = str_replace('-', '/', $path);
        download_file('', FCPATH .$path);
    }

//    private function update_order_detail_to_download_shipping_file($order_detail)
//    {
//        $this->load->model(array('app_common'));
//        $foreign_key_array = array('id' => explode(', ', $order_detail));
//        $params = array(
//            'shipping_status_code'  => fetch_code('shipment_status_waiting'),
//            'instruction_plan_date' => date('Y-m-d'),
//            'cancel_datetime'       => App_common::DATE_TYPE_YMD_HIS,
//            'authorize_status'      => '1'
//        );
//
//        return $this->app_common->update_item_by_by_foreign_key_array('order_detail', $foreign_key_array, $params);
//    }
}

/* End of file tools.php */
/* Location: {module_location}/_staff/tools/debug_shipping_file.php */
