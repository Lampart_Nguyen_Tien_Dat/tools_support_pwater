<div>
    {$result}
</div>
<script>
    $(document).ready(function () {
        $('.zip').click(function () {
            var cls = $(this).attr('class').replace('zip ', '');
            cls = cls.replace(/\//g, '-');
            var file_name = $(this).text();
            var path = cls + file_name;
            document.location = MOD_URL + "tools/debug_shipping_file/download_file/path/" + path;
        });
    });
</script>
<style>
    li {
        margin: 5px 0px 5px 0px;
    }
</style>