<div align="center">
    <div>
        <h1>Update order_detail to download shipping_file</h1>
        <form action="" method="post">
            <input type="textbox" name="order_detail_id" placeholder="1, 2, 3, 4...."/>
            <input type="submit" value="Update"/>
        </form>
    </div>

    <div style="margin: 50px 0 50px 0;">
        <h1>Modify shipping file</h1>
        <form action="" method="post">
            <input type="date" name="day" min="2015-01-01">
            <input type="submit" value="Modify">
        </form>
    </div>

    <div style="font-size: x-large; color: red;">
        {if $result}
            {$result}
        {/if}
    </div>
</div>