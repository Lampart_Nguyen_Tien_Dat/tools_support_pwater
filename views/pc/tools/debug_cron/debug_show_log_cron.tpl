<br>
<div align="center">
    <table>
        <tr>
            <th>QC name</th>
            <th>Function</th>
            <th>Create date time</th>
            <th>Result</th>
            <th>Account</th>
        </tr>
        {foreach $param item}
            <tr>
                <td>{$item['qc_name']}</td>
                <td>{$item['function']}</td>
                <td>{$item['create_datetime']}</td>
                <td>{$item['result']}</td>
                <td>{$item['account']}</td>
            </tr>
        {/foreach}
    </table>
    <div class="display_table">
        {$pagination}
    </div>

</div>

<style>
    table {
        border: solid;
    }
    tr, td, th {
        border: solid;
        padding: 10px;
    }
    th {
        background-color: pink;
    }
</style>
