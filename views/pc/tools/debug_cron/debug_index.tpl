{$logout}
<div align="center">
    <form action ="" method="post">
    {if $content}
        Tên QC: <input type="text" name="qc_name"/><br><br>
    {if $link}
        {$link}<br><br>
    {/if}
    {/if}
    <table >
        <tr>
            <th>Cron</th>
            <th>Function</th>
            <th>Cron đã chọn</th>
        </tr>
        <tr>
            <td >{$menu}</td>
            <td width="450">{$content}</td>
            <td width="450" id="checked_cron"></td>
        </tr>
    </table>
    {if $content}
        <br><input type="submit" name="submit">
    {/if}
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var crurrent_cron = 0;
        $(".grade_1").on("click", "li.item_menu", function (event) {
            crurrent_cron = event.target.id;
            $(".func p").hide();
            $(".func p."+ crurrent_cron).show();
        });

        $('input[type=checkbox]').on('change',function(){
            var check = [];
            check = check_checkbox();
            $("#checked_cron").empty();
            for(var i=0; i<check.length; i++){
                $("#checked_cron").append("<p style='color: #00b1f2'>" +  check[i] + "</p>");
            }
        });

        function check_checkbox() {
            var checked = [];
            $('input[type=checkbox]').each(function (){
                {
                    if(this.checked){
                        checked.push($(this).val());
                    }
                }
            });
            if(checked.length != 0){
                $("li#" + crurrent_cron).css("color", "#fc72ff");
            }else{
                $("li#" + crurrent_cron).css("color", "");
            }
            return checked;
        }
    });
</script>
<style>
    table {
        border: solid;
    }
    tr, td, th {
        border: solid;
        padding: 10px;
    }
    th {
        background-color: pink;
    }
    .item_menu{
        cursor: pointer;
    }
</style>